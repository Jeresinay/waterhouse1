// Import the functions you need from the SDKs you need
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDomD3BABlSmX75HI3--r8Idhgih2zEFW8",
    authDomain: "waterhouse-b4c24.firebaseapp.com",
    projectId: "waterhouse-b4c24",
    storageBucket: "waterhouse-b4c24.appspot.com",
    messagingSenderId: "928177087601",
    appId: "1:928177087601:web:198571ad764b05bc3da7d0"
  };
//inicializo firebase 
const app = initializeApp(firebaseConfig);

const auth = getAuth();

console.log("Bienvenido a la consola de pruebas.");
// Serial.println("...");

var lcorreoRef = document.getElementById("lcorreoId");
//console.log(lcorreoRef);

var lpasswordRef = document.getElementById("lpasswordId");
//console.log(lpasswordRef);

var ingresarRef = document.getElementById("ingresarbotonId");

//llamo a las funciones con un evento
ingresarRef.addEventListener("click", logIn);

// Creo la funcion logIn
function logIn ()
{

    if((lcorreoRef.value != '') && (lpasswordRef.value != '')){

        signInWithEmailAndPassword(auth, lcorreoRef.value, lpasswordRef.value) 
        .then((userCredential) => {
            const user = userCredential.user;
             // si esta todo correcto se direcciona al usuario a la página de inicio
            window.location.href = "../index.html";
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            alert("Revise el usuario o la contraseña")
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos esten completos.");
    }    
}



